+++
date = "2017-04-07"
draft = false
image = "banners/getting-started.png"
tags = ["life", "status"]
title = "Hello, World."
math = true
summary = "In which I introduce myself and my goals for this blog."
+++

## Choosing Myself

When I started this blog post in November 2016, I realized I need to provide my own value and offer it to the world, instead of relying on that one person's yes or no to assign a value to my time and effort. I then proceeded to spend the next 4-5 months not really doing much of that - thanks, Rocket League.. Emotional inertia is a real thing. I was probably asking for more money than the companies I was interviewing with were willing to pay for the jobs they needed done, and I didn't get them. I couldn't really ask for less than I was, without continuing to dig a bigger financial hole, faster than I had while working for my last employer, but I also wasn't really doing much to add the skills I wanted or succeed with the projects I had started.

Back then (November '16), a video of this hairbrush-deficient guy popped up on my Facebook feed, and I watched a little of it. James Altucher will send you his book "Choose Yourself" in return for your email address and a small fee for shipping and handling. I thought about it, but passed. A few days later I noticed the book available for free in Amazon Prime reading, so I sent it to my kindle library and opened it up on my phone. [Choose Yourself](https://amzn.to/2fYijBW) by James Altucher is a common-sense, somewhat-scientifically-informed approach to the problem of modern life. You can get it free too with [Amazon Prime reading](https://www.amazon.com/tryprimefree?ref_=assoc_tag_ph_1427739975520&_encoding=UTF8&camp=1789&creative=9325&linkCode=pf4&tag=bmackinney0c-20&linkId=7011c09c504f63985ea8959d768e1258).

I enjoyed the book, even if it was basically a collection of Mr. Altucher's blog posts and summaries of some of the lessons he's learned interviewing various people. I liked it enough to read it in a couple of days and start this post to proclaim I would be following James' lead and writing for fun and profit. The post read,

> This post is an announcement to the world, that I am still alive, that I love myself, and that I'm going to share myself openly. Mr. Altucher recommends writing. I'm going to blog. I've always been a good writer, and I need to exercise that muscle. I've been writing for a few days straight now, and I'm happy with my progress. I decided to start a book about the self-fulfilling prophecy, and I've got a general plan and a draft started with 1,700 words so far.

I then didn't finish or publish the post, failing to commit to starting the journey I then and now wish I had. It's ok. I accept this about myself. Publishing a personal site is intimidating, but I'm over it. It's time to read and write for fun and profit. 

This is the first real post I've written and published. What good are my ideas, if I assume they'll be rejected and thus never share them with anyone? I have good ideas. I know and believe some interesting and important things.  I need to connect with you, because I think it will make both of our lives better. I listen to 311 a lot. The song "Golden Sunlight" has always inspired thoughts of living out in the open, of writing what I think and do and say, and then sharing it with the world via this big crazy thing we call the internet. Enjoy!

{{< spotify 0hH0dRLBpSaDcSsPltjqUM >}}
