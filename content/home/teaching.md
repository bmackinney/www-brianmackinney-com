+++
# An example of using the custom widget to create your own home page section.
# To create more sections, duplicate this file and edit the values below as desired.

date = "2016-04-20T00:00:00"
draft = false

title = "I am a human who..."
subtitle = ""

# Order that this section will appear in.
weight = 20

+++

- believes in <a href="https://amzn.to/2Lh1kv8">reason, science, humanism, and progress</a>, <a href="https://www.capitalism.org/">capitalism</a>, <a href="https://en.wikipedia.org/wiki/Libertarianism">liberty</a>, and <a href="/post/why-i-am-an-atheist/">skepticism</a>.
- goes to <a href="https://fangraphs.com/teams/diamondbacks">Arizona Diamondbacks games.</a>
- is male, 36, 6', and 180#.
- prefers <a href="https://en.wikipedia.org/wiki/FOSS">Free and Open Source Software</a> (and content).
- calls <a href="https://en.wikipedia.org/wiki/Arizona">Arizona</a> home.
- started <a href="https://www.basa.casa">Basa Casa</a> to make life better for everyone.
- uses a <a href="https://apple.com/mac/">Mac</a>
- doesn't write regularly, but wishes he would.

