+++
# Contact widget.

date = "2016-04-20T00:00:00"
draft = false

title = "Contact"
subtitle = "There are two ways to get in touch with me, and both are here on GitLab."
widget = "contact"

# Order that this section will appear in.
weight = 90

# Automatically link email and phone?
autolink = true

+++

1. [Ask me anything](https://gitlab.com/bmackinney/ama/issues) in public, on my Gitlab project's issue queue.
1. Send me a short message here.
<form name="contact" netlify-honeypot="bot-field" action="thank-you" netlify>
<p class="hidden">
  <label>Don’t fill this out: <input name="bot-field"></label>
</p>
<p>
  <label>Email: <input type="text" name="name"></label>
</p>
<p>
  <label>Message: <textarea name="message"></textarea></label>
</p>
<p>
  <button type=”submit”>Send</button>
</p>
</form>
1. Find me around:

![picture of a map of the Phoenix area with dots indicating my google location history](img/map.png)
