+++
# About/Biography widget.

date = "2016-04-20T00:00:00"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 10

# List your academic interests.
[interests]
  title = "Interests"
  interests = [
    "Evolutionary Psychology",
    "Economics",
    "Instructional Design",
    "Open Source"
  ]

# List your qualifications (such as academic degrees).
[education]
  title = "Education"

[[education.courses]]
  course = "Master of Educational Technology"
  institution = "Arizona State University"
  year = 2007

[[education.courses]]
  course = "BS in Secondary Education"
  institution = "The Original Grand Canyon University"
  year = 2004

+++

## Bio

I'm not very good at anything, which is frustrating to me. I haven't had enough practice, and didn't take advantage of every opportunity I've had to develop my skills. It's why I started a company called [BasaCasa, Inc](https://www.basa.casa) - to make the world, starting with myself and those around me, more competent. We'll do it together by learning to implement Four-Component Instructional Designs! You can give me a vote of confidence there with as little as $1 towards the company's [IndieGoGo campaign - Launch BasaCasa - because it's fun to say](https://www.indiegogo.com/launch-basa-casa).

I live near where I grew up, in south Tempe AZ, by way of San Diego, Chandler, Honolulu, Chandler again, and San Francisco. Professionally I've failed more than I have succeeded, and I'm probably going to stop trying and go into something else. My favorite tools are [Hugo](https://www.gohugo.io/), [Four-Component Instructional Design](https://tensteps.info), and [GitLab](https://about.gitlab.com). My goal was always to write and publish at least one thing a day here.. but I have failed at that so far. Here goes another attempt.



