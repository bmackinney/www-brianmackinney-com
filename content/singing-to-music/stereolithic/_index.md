+++
date = "2017-06-09T14:22:02+10:00"
image = ""
math = false
tags = []
title = "Stereolithic"

+++
311's 2014 EP was released right before I moved from Chandler to San Francisco. This was an exciting time for me and my family. I associate much of the album with drunken rides on the muni and to a lesser degree, the BART. With the impending release of Mosaic, I want to capture my thoughts and emotions surrounding Stereolithic, and to start a tradition of writing about the music that has moved me throughout the years.

{{ spotify stereolithic }}

From the wind-up that starts Ebb and Flow, to the final long-held notes and aw-oo's of Tranquility, I consider Stereolithic 311's best work of art. It makes perfect sense to me, that a band who has been together for 26 years would get better as time moves forward. Over the next several days, I'll write a quick post about each of the album's fifteen songs, and then do the same for 2017's Mosaic.
